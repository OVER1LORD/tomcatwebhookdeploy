<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>E-Commerce Website</title>
    <style>
        /* Basic styling for the header */
        header {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        /* Styling for product cards */
        .product-card {
            border: 1px solid #ddd;
            border-radius: 5px;
            margin: 10px;
            padding: 10px;
            text-align: center;
            background-color: #fff;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            display: inline-block;
            width: 250px;
        }

        /* Styling for product images */
        .product-image {
            max-width: 100%;
            height: auto;
        }

        /* Styling for product titles */
        .product-title {
            font-size: 18px;
            margin: 10px 0;
        }

        /* Styling for product prices */
        .product-price {
            font-size: 16px;
            color: #333;
        }
    </style>
</head>
<body>
    <header>
        <h1>Welcome to Our E-Commerce Store</h1>
    </header>

    <div class="product-card">
        <img class="product-image" src="product1.jpg" alt="Product 1">
        <h2 class="product-title">Product 1</h2>
        <p class="product-price">$19.99</p>
        <button>Add to Cart</button>
    </div>

    <div class="product-card">
        <img class="product-image" src="product2.jpg" alt="Product 2">
        <h2 class="product-title">Product 2</h2>
        <p class="product-price">$29.99</p>
        <button>Add to Cart</button>
    </div>

    <!-- More product cards can be added here -->

</body>
</html>
